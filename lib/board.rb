class Board
  attr_reader :grid

  def initialize(grid = (Array.new(3) { Array.new(3)}))
    @grid = grid
    @winning_mark = nil
  end

  #Because of the following two methods, we can now use this notation:
  #      board[pos] = :X  or  board[pos] #=> :X
  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end

  def place_mark(pos, mark)
    if empty?(pos)
      self[pos] = mark
    else
      raise Exception
    end
  end

  def empty?(pos)
    self[pos] == nil
  end

  def winner
    @winning_mark if over?
  end

  def over?
    horizontal || vertical || diagonal || cats_game
  end

  def horizontal
    bool1 = @grid[0][0] != nil && (@grid[0][0] == @grid[0][1] && @grid[0][1] == @grid[0][2])
    @winning_mark = @grid[0][0] if bool1 == true
    bool2 = @grid[1][0] != nil && (@grid[1][0] == @grid[1][1] && @grid[1][1] == @grid[1][2])
    @winning_mark = @grid[1][0] if bool2 == true
    bool3 = @grid[2][0] != nil && (@grid[2][0] == @grid[2][1] && @grid[2][1] == @grid[2][2])
    @winning_mark = @grid[2][0] if bool3 == true
    bool1 || bool2 || bool3
  end

  def vertical
    bool1 = @grid[0][0] != nil && (@grid[0][0] == @grid[1][0] && @grid[1][0] == @grid[2][0])
    @winning_mark = @grid[0][0] if bool1 == true
    bool2 = @grid[0][1] != nil && (@grid[0][1] == @grid[1][1] && @grid[1][1] == @grid[2][1])
    @winning_mark = @grid[0][1] if bool2 == true
    bool3 = @grid[0][2] != nil && (@grid[0][2] == @grid[1][2] && @grid[1][2] == @grid[2][2])
    @winning_mark = @grid[0][2] if bool3 == true
    bool1 || bool2 || bool3
  end

  def diagonal
    bool1 = @grid[0][0] != nil && (@grid[0][0] == @grid[1][1] && @grid[1][1] == @grid[2][2])
    @winning_mark = @grid[0][0] if bool1 == true
    bool2 = @grid[0][2] != nil && (@grid[0][2] == @grid[1][1] && @grid[1][1] == @grid[2][0])
    @winning_mark = @grid[0][2] if bool2 == true
    bool1 || bool2
  end

  def cats_game
    !@grid[0].include?(nil) && !@grid[1].include?(nil) && !@grid[2].include?(nil)
  end
end
