class ComputerPlayer
  attr_reader :board, :name
  attr_accessor :mark

  def initialize(name)
    @name = name
    @mark = :O
  end

  def display(board)
    @board = board
  end

  def get_move
    pos = nil
    changed = false

    (0..2).each do |r|
      (0..2).each do |c|
        if @board.empty?([r,c])
          @board[[r,c]] = @mark
          changed = true
        end
        pos = [r,c] if @board.over?
        @board[[r,c]] = nil if changed == true
        changed = false
      end
    end

    if pos == nil
      pos = [rand(0..2),rand(0..2)]
      until @board.empty?(pos)
        pos = [rand(0..2),rand(0..2)]
      end
    else
      pos
    end
    pos
  end
end
