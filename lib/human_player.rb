class HumanPlayer
  attr_reader :name, :mark
  def initialize(name)
    @name = name
    @mark = :X
  end

  def display(board)
    p board.grid[0]
    p board.grid[1]
    p board.grid[2]
    puts ""
  end

  def get_move
    puts "Where do you want to move? (Enter in the form: 0,0)"
    input = gets.chomp
    pos = [input[0].to_i,input[-1].to_i]
  end
end
