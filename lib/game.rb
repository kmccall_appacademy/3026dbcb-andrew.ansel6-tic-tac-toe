require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_reader :board, :current_player

  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    @current_player = @player_one
    @board = Board.new
  end

  def switch_players!
    if @current_player != @player_one
      @current_player = @player_one
    else
      @current_player = @player_two
    end
  end

  def play_turn
    @current_player.display(@board)
    @board.place_mark(@current_player.get_move, @current_player.mark)
    @current_player.display(@board)
    switch_players!
  end

  def play
    until @board.over?
      play_turn
    end

    puts "Congrats! The winner is: #{@board.winner}"
  end
end

# #Use this to test
# newgame = Game.new(HumanPlayer.new("human"), ComputerPlayer.new("computer"))
# newgame.play
